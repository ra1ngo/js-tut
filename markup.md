# Верстка #

### Задание ###
Раньше верстали только через table, что было плохо с точки зрения оптимизации. Затем float с костылями. 
Flexbox стал спасением от костылей. Grid'ы пошли еще дальше. Однако, рекомендую начать именно с flexbox. 
Т.к. на реакт натив используются именно они и есть возможность быстро без переобучения начать кодить мобилки.
Плюс, многие проекты пока еще не юзают гриды.

Хочется придумать задание на историю. Сверстать что-то одно на таблицах, флоутах, флексах, гридах. 
https://qna.habr.com/q/89477

### Ссылки ###
    //сборники ссылок на тренажеры
    https://codepip.com/
    https://www.instagram.com/p/B-O5wbviKU5/?igshid=wzftk2sp2t6u
    https://techrocks.ru/2019/11/20/12-free-games-for-css-learning/
    //тренажеры
    http://flukeout.github.io/
    http://ru.learnlayout.com/
    https://flexboxfroggy.com/#ru
    https://cssgridgarden.com/#ru
    https://cssbattle.dev/

достаточно прикольный способ учиться - изучать вопросы стэковерфлоу:
https://stackoverflow.com/questions/tagged/css?tab=Votes

примеры кода 
https://codepen.io/popular/pens/

    //блоги
    https://css-live.ru/
    https://frontender.info/
    http://prgssr.ru/

    //статьи
    https://htmlacademy.ru/blog/boost/frontend
    типографика: https://2web-master.ru/rukovodstvo-po-google-font-api.html
    
    //стайлгайды 
    https://netology-university.bitbucket.io/codestyle/
    https://habr.com/ru/post/143452/

    //ютуб каналы
    https://www.youtube.com/watch?v=IPYAJIGMqr4&list=UUn-P_F0tfY21cfnkyv2lsRQ&index=15

### Задание 1 ###
Было бы прикольно составить список простеньких заданий по верстке для изучения.

Создать резиновую верстку с несколькими блоками, расположенных в ряд. Так, чтобы между блоками были равные промежутки.
А снаружи промежутков не было. Т.е. слева и справа блоки прилегают к границе внешнего элемента.
Ряд блоков должен заполнять все пространство по ширине внешнего элемента. Блоки должны быть резиновыми, 
не иметь фиксированную ширину.

Промежутки могут быть фиксированными.

https://codepen.io/ra1ngo/pen/ZEbBMeJ?editors=1100
https://stackoverflow.com/questions/20626685/better-way-to-set-distance-between-flexbox-items

### Задание 2 ###
Сделать так, чтобы каждый второй див был определенного цвета, кроме дивов определенных классов.
