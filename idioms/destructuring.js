'use strict';

// Деструктуризация массивов
let [firstName, lastName] = ["Илья"];   //lastName = undefined
let [, middleElem] = ["Илья", "Кантор", "Физик"]; //middleElem = Кантор
let [, ...rest] = ["Илья", "Кантор", "Физик", "Ядерщик"]; //rest = [ 'Кантор', 'Физик', 'Ядерщик' ]
let [, lastNameWithDefaultValue = "Кантор"] = ["Илья"];   //lastNameWithDefaultValue = Кантор
// Деструктуризация объектов
let {name, surname} = {name: "Илья", surname: "Кантор"};
let {surname: s} = {name: "Илья", surname: "Кантор"};     //s = Кантор
let {lastName: l="Кантор"} = {name: "Илья"};     //l = Кантор
//let [v1, v2] = {firstName: "Илья", lastName: "Кантор"};  //так нельзя
//let [v1, v2] = {v1: "Илья", v2: "Кантор"}; //так тоже
//можно так:
let arr = ['1','2'];
let {0:a, 1:b} = arr;  //a = '1', b = '2'

// Присвоение остатка в деструктуризации объекта
let options = {
  title: "Меню",
  width: 100,
  height: 200
};
let {title, ...size} = options; //size - { width: 100, height: 200 }

// Вложенная деструктуризация
let options2 = {
  size: {
    width: 100,
    height: 200
  },
  items: ["Пончик", "Пирожное"]
}
let { size: {width, height}, items: [item1, item2] } = options2;
