'use strict';

const getCar = (make, model, value) => ({
    make,  // аналогично make: make

    // вычисляемые свойства теперь работают в
    // литералах объекта
    ['model' + model]: value,

    // Короткая запись метода объекта пропускает
    // ключевое слово `function` и двоеточие. Вместо
    // "depreciate: function() {}" можно написать:
    depreciate() {
      this.value -= 2500;
    }
})

let car = getCar('Kia', 'Sorento', 40000);
console.log(car); //{ make: 'Kia', modelSorento: 40000, depreciate: [Function: depreciate] }
