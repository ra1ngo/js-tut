'use strict';

let arr = ['apple', 'banana', 'orange'];

// Плохо
let bad = arr.map((fruit) => {
  return fruit + 's';
});

// Плохо
let good = arr.map(fruit => fruit + 's');

// Если нужно вернуть объект, то заключаем в скобки, чтобы компилятор не подумал, что идет блок функции
let obj = arr.map(fruit => ({fruit}));
console.log(obj); // ['apples', 'bananas', 'oranges']
