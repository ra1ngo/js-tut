'use strict';

const pets = ['cat', 'dog', 'bat'];
console.log(pets.includes('cat'));  //true
console.log(pets.includes('cat', 1));  //false

const arr = [1, 2, [3, 4, [5, 6]]];
console.log(arr.flat()); // [1, 2, 3, 4, [5, 6]]

const arr2 = [1, 2, [3, 4, [5, 6, [7, 8, [9, 10]]]]];
console.log(arr2.flat(Infinity)); // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

const array = [1, 2, 3, 4, 5];
const even = element => element % 2 === 0;
console.log(array.some(even));  //true
console.log(array.every(even)); //false
