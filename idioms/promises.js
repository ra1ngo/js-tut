'use strict';

new Promise(resolve => setTimeout(resolve, 10000));

const hello = new Promise((resolve, reject) => resolve("Привет"));
hello.then(str => console.log(`${str}, Мир`));

const f1 = () => {
  new Promise(resolve => setTimeout(resolve, 1000));
  console.log('я выполнился сразу');
}

// Await заставляет выполняться промис
const f2 = async () => {
  await new Promise(resolve => setTimeout(resolve, 1000));
  console.log('я выполнился через секунду');
}

f1();
f2();
